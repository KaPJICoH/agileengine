<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;                
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }
            
            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }
            
            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            button {
                width: 100px;
                height: 30px;
            }
            body {
                margin: 15px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <?php if ($products): ?>
                <table id="product_table" >                
                    <thead>
                        <tr>
                            <th class='sort' type="string">Название Товара</th>
                            <th class='sort' type="number">Цена</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>                    
                        <?php foreach ($products as $product): ?>
                            <tr>
                                <td>{{$product->name}}</td>
                                <td>{{$product['discount_price']}}</td>
                                <td> 
                                {!! Form::open(array('url'=> '/api/buy/product/'.$product->id, 'method' => 'get')) !!}                     
                                    {!! Form::submit('Buy') !!}
                                {!! Form::close() !!}</td>
                            </tr>
                        <?php endforeach ?>                                         
                    </tbody>
                </table>
            <?php else: ?> 
                <h1>Вы еще не добавили товар в этот список</h1> 
            <?php endif ?> 
        </div>
    </body>
        <script>
        $('form').submit(function(e){
            e.preventDefault();            
            var furl=$(this).attr('action');
            ajax_request=$.ajax({
                url: furl,               
                success: function(e)
                {
                    alert('Product was added');
                },
                error: function(e)
                {
                    var responce = JSON.parse(e.responseText);                    
                    alert(responce["errors"][0].message)
                }
            });
        })
                
    </script>
    <script>
        var product_table = document.getElementById('product_table');

        product_table.onclick = function(e) {
            if (e.target.classList.contains('sort')){
                sortTable(e.target.cellIndex, e.target.getAttribute('type'));
            }
        };

        function sortTable(colNum, type) {
            var tbody = product_table.getElementsByTagName('tbody')[0];      
            var rowsArray = [].slice.call(tbody.rows);
            var compare;

            switch (type) {
                case 'number':
                    compare = function(rowA, rowB) {
                        return rowA.cells[colNum].innerHTML - rowB.cells[colNum].innerHTML;
                    };
                    break;
                case 'string':
                    compare = function(rowA, rowB) {
                        return rowA.cells[colNum].innerHTML > rowB.cells[colNum].innerHTML ? 1 : -1;
                    };
                    break;
            }
          
            rowsArray.sort(compare); 

            product_table.removeChild(tbody); 

            for (var i = 0; i < rowsArray.length; i++) {
                tbody.appendChild(rowsArray[i]);
            }
            
            product_table.appendChild(tbody);
        }
    </script>
</body>
</html>
