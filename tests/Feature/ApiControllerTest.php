<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Discount;
use App\Product;
use App\Voucher;

class ApiControllerTest extends TestCase
{
	private $product;

	private $voucher;

    /**
     * A basic test example.
     *
     * @return void
     */

   	public function testCreateProduct()
	{
		$response = $this->call('POST', '/api/product', array('name' => 'test' ,'price' => '1200', '_token' => csrf_token()) );
		$data = $response->original['data'];		
		$this->assertEquals('test', $data['name']);		
	}

	public function testCreateProductWithoutName()
	{
		$response = $this->call('POST', '/api/product', array( 'price' => '1200', '_token' => csrf_token()) );				
		$this->assertEquals(400, $response->status());
	}

	public function testCreateProductWithoutPrice()
	{
		$response = $this->call('POST', '/api/product', array( 'name' => 'asdads', '_token' => csrf_token()) );		
		$this->assertEquals(400, $response->status());
	}

	public function testCreateVoucher()
	{		
		$discount = Discount::all()->first(); 		
		$response = $this->call('POST', '/api/voucher', array(
			'date_start' => '1994-04-12',
			'date_end' => '1994-04-12', 
			'discount_id' => $discount->id,
			'_token' => csrf_token()) 
		);					
		$this->assertEquals(200, $response->status());
	}

	public function testCreateVoucherWithWrongDiscount()
	{

		$response = $this->call('POST', '/api/voucher', array(
			'date_start' => '1994-04-12',
			'date_end' => '1994-04-12', 
			'discount_id' => 1200,
			'_token' => csrf_token()) 
		);				
		$this->assertEquals(404, $response->status());	
	}

	public function testCreateVoucherWithWrongDate()
	{	
		$discount = Discount::all()->first();
		$response = $this->call('POST', '/api/voucher', array(
			'date_start' => '12132',
			'date_end' => 'asdadsdas', 
			'discount_id' => $discount->id,
			'_token' => csrf_token()) 
		);						
		$this->assertEquals(400, $response->status());	
	}

	public function testMakeDepend()
	{		
		$this->product = Product::all()->first();
		$this->voucher  = Voucher::all()->first();  		
		$response = $this->call('get', '/api/depend/voucher/'.$this->voucher->id.'/product/'.$this->product->id);					
		$this->assertEquals(200, $response->status());

		$response = $this->call('delete', '/api/depend/voucher/'.$this->voucher->id.'/product/'.$this->product->id);					
		$this->assertEquals(200, $response->status());
	}

	public function testMakeDependWithWromdData()
	{					
		$response = $this->call('get', '/api/depend/voucher/adsasd/product/asd');					
		$this->assertEquals(400, $response->status());
	}

	public function testDeleteDependWithWromdData()
	{	  		
		$response = $this->call('get', '/api/depend/voucher/adsasd/product/asd');					
		$this->assertEquals(400, $response->status());
	}

	public function testBuyProduct()
	{	 
		$product = Product::where('status' , '=' , 1)->first();
		$response = $this->call('get', '/api/buy/product/'.$product->id);					
		$this->assertEquals(200, $response->status());		
	}

	public function testBuyProductWrongId()
	{			
		$response = $this->call('get', '/api/buy/product/asddas');					
		$this->assertEquals(404, $response->status());
	}

	public function testBuyProductWithAlredySold()
	{
		$product = Product::where('status' , '===' , 0)->first();			
		$response = $this->call('get', '/api/buy/product/'.$product->id);					
		$this->assertEquals(403, $response->status());
	}
}
