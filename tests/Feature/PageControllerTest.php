<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PageControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
   	public function testHome()
	{
	    $response = $this->call('GET', '/');
	    $view = $response->original;
		$this->assertContainsOnly('object', $view['products']);
	    $this->assertEquals(200, $response->status());
	   
	}
}
