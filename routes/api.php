<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => 'api'], function()
{
	Route::post('/product', [
		'as'=>'post.productt',
	    'uses'=>'ApiController@postProduct'
	]);

	Route::post('/voucher', [
		'as'=>'post.voucher',
	    'uses'=>'ApiController@postVoucher'
	]);

	Route::get('/depend/voucher/{voucher_id}/product/{product_id}', [
		'as'=>'get.depend',
	    'uses'=>'ApiController@getDepend'
	]);

	Route::delete('/depend/voucher/{voucher_id}/product/{product_id}', [
		'as'=>'delete.depend',
	    'uses'=>'ApiController@deleteDepend'
	]);

    Route::get('/buy/product/{id}', [
		'as'=>'buy.product',
	    'uses'=>'ApiController@buyProduct'
	]);
});
