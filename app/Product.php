<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'price'
    ];

    public function vouchers()
    {
        return $this->belongsToMany('App\Voucher', 'voucher_to_product', 'product_id', 'voucher_id');
    }

    public function hasVoucher($voucher_id){        
        return $this->vouchers->contains('id', $voucher_id);       
    }

    public function assign(Voucher $voucher){
        if (!$this->hasVoucher($voucher->id)) {
            return $this->vouchers()->save($voucher);
        }        
    }

    public function unassign(Voucher $voucher){
        if ($this->hasVoucher($voucher->id)) {
            return $this->vouchers()->detach($voucher);
        }        
    }
}


    

    

   
    