<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    protected $table = 'voucher';

    public $timestamps = false;

    protected $fillable = [
        'date_start',
        'date_end',
        'discount_id'
    ];

    public function discount()
    {
        return $this->belongsTo('App\Discount');
    }
}
