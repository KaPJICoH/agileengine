<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    protected $table = 'discount';

    public $timestamps = false;

    protected $fillable = [        
        'discount'
    ];

    public function Voucher()
	{
	    return $this->HasOne('App\Voucher');
	}
}
