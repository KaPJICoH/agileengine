<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Resourse\VoucherDiscount;
use App\Product;
use App\Discount;
use App\Voucher;
use App\Resourse\BuyException;


class ApiController extends Controller
{	

    protected $statusCode = 200;
   
    protected $errors = [];

    protected $response = [];

    private $voucherDiscount;

    public function __construct(){
        $this->voucherDiscount = new VoucherDiscount();
    }  

    private function addError($error)
    {
    	$this->statusCode = $error->getCode()? $error->getCode(): 400;    		
		array_push($this->errors, [
                    'status_code' => $this->statusCode,
                    'message' => $error->getMessage()                        
                ]);
    }
    private function getResponse()
    {
    	return array(
            'data' => $this->response,
            'errors' => $this->errors,
            'status_code' => $this->statusCode
        );
    }

    public function postProduct(Request $request)
    {
        try {
            $this->response = $this->createProduct($request['name'] , $request['price']);
        } catch (\Exception $e) {
            $this->addError($e);
        } finally {
            return response()->json(
                $this->getResponse(),
                $this->statusCode
            );
        }
    }

    private function createProduct($name, $price)
    {
        try {
            return Product::create([
                'name' => $name,
                'price'=> $price
            ]);
        } catch (\Exception $e) {
            throw new \Exception("Something wrong with you data, check it", 400);                
        }       
    }

    public function postVoucher(Request $request)
    {
       try {
            $discount = $this->getDiscountById($request['discount_id']);
            $this->response = Voucher::create([
                'date_start' => $this->validateTime($request['date_start']),
                'date_end' => $this->validateTime($request['date_end']),
                'discount_id' => $discount->id
                ]);            
        } catch (\Exception $e) {
            $this->addError($e);
        } finally {
            return response()->json(
                $this->getResponse(),
                $this->statusCode
            );
        } 
    }

    private function getDiscountById($id){
        try {
            return Discount::findOrFail($id);
        } catch (\Exception $e) {
            throw new \Exception("Discount didn't found", 404);            
        }        
    }
    
    private function validateTime($time)
    {
        if ($timestamp = strtotime($time)) {
            return date("Y-m-d", $timestamp);
        } else {
            throw new \Exception("Wrong time format. Try 'YYYY-mm-dd' fromat", 400);           
        }
    }
   
    public function getDepend($vouche_id, $product_id)
    {
        try {            
            $this->assign($vouche_id, $product_id);
            $this->response = 'Voucher was assigned to the product';
        } catch (\Exception $e) {
            $this->addError($e);
        } finally {
            return response()->json(
                $this->getResponse(),
                $this->statusCode
            );
        }     	
    }

    private function assign($vouche_id, $product_id)
    {
        try {
            $product = Product::findOrFail($product_id);
            $voucher = Voucher::findOrFail($vouche_id);
            $product->assign($voucher);

        } catch (\Exception $e) {
            throw new \Exception("Something wrong with you data, check it", 400);            
        }        
    }

    public function deleteDepend($vouche_id, $product_id)
    {
        try {            
            $this->unassign($vouche_id, $product_id);
            $this->response = 'Voucher was unassigned from the product';
        } catch (\Exception $e) {
            $this->addError($e);
        } finally {
            return response()->json(
                $this->getResponse(),
                $this->statusCode
            );
        } 
    }

    private function unassign($vouche_id, $product_id)
    {
        try {
            $product = Product::findOrFail($product_id);
            $voucher = Voucher::findOrFail($vouche_id);
            $product->unassign($voucher);
        } catch (\Exception $e) {
            throw new \Exception("Something wrong with you data, check it", 400);            
        }        
    }

    public function buyProduct($id)
    {
        try {
            $this->response = $this->getProduct($id);          
        } catch (BuyException $e) {
            $this->addError($e);
        } finally {
            return response()->json(
                $this->getResponse(),
                $this->statusCode
            );
        } 
    }
   
    private function getProduct($id)
    {
        try {
            $product = Product::findOrFail($id);
            if ($product->status) {
                $productData = $this->getProductData($product); 
                $this->changeProductStatus($product);
                return $productData;        
            } else {
                throw new BuyException("Product alredy sold", 403); 
            }
        } catch (BuyException $e) {
            throw new BuyException($e->getMessage(), $e->getCode());            
        } catch (\Exception $e) {
            throw new BuyException($e->getMessage(), 404);
        }
    }

    private function getProductData($product)
    {   
        
        $discountPrice = $this->voucherDiscount->getDiscoountPrice($product);
        return array(
                'name' => $product->name,
                'price' => $product->price,
                'discount_price' => $discountPrice
            );        
    }

    private function changeProductStatus($product)
    {
        $product->status = false;
        foreach ($product->vouchers as $voucher) {
            $voucher->status = false;
            $voucher->save();
        }
        $product->save();
    }    
}
