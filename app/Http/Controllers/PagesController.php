<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Resourse\VoucherDiscount;

class PagesController extends Controller
{
    /**
     * Return home page with list of product
     *
     * @return Response
    */  
    public function getHome()
    {
    	$products = Product::all();

        foreach ($products as $product) {
            $voucherDiscount = new VoucherDiscount();           
            $product['discount_price'] = $voucherDiscount->getDiscoountPrice($product);
        }

        return view('home', compact('products'));
    }
}
