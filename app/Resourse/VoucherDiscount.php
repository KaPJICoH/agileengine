<?php

namespace App\Resourse;

/**
* 
*/
class VoucherDiscount
{	
	public function getDiscoountPrice($product)
    {    	
    	return $product->price * ( 100 - $this->getDiscount($product) ) / 100;
    }

    public function getDiscount($product)
    {   
        $discount = 0;        	
    	foreach ($product->vouchers as $voucher) {
    		$discount +=  $this->getDiscoindIfVoucherAvailable($voucher);
    	}
        return ($discount < 60) ? $discount : 60;
    }

    private function getDiscoindIfVoucherAvailable($voucher)
    {
        $today = date('Y-m-d'); 
        if ($voucher->status && $voucher->date_start <= $today && $voucher->date_end >= $today) {
            return $voucher->discount->discount;//todo
        }        
    }
}