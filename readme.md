# How start #

For start working with this project you need make 3 steps:

- Create .env file. You can found example this file in main directory.
- Run migreate.
- For create fake data you can use "db:seed".

# About API #

## For create product ##

### post:"/api/product" ###
* *param $name.*
* *param $price.*

## For create voucher ##

### post:"/api/voucher" ###
* *param $data_start.*
* *param $data_end.*
* *param $discount_id.*

## For add voucher to product ##

### get:"/api/depend/voucher/{voucher_id}/product/{product_id}" ##

## For remove voucher from product ##

### delete:"/api/depend/voucher/{voucher_id}/product/{product_id}" ##

## For buy product ##

### get:"/api/buy/product/{product_id}" ##