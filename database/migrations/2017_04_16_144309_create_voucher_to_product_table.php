<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVoucherToProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voucher_to_product', function (Blueprint $table) {
            $table->integer('voucher_id')->unsigned();
            $table->integer('product_id')->unsigned();

            $table->foreign('voucher_id')
                ->references('id')->on('voucher')
                ->onDelete('cascade');
                
            $table->foreign('product_id')
                ->references('id')->on('product')
                ->onDelete('cascade');               
        });   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voucher_to_product');
    }
}
