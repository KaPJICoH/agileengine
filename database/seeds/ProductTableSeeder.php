<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('product')->delete();

		$faker = Faker\Factory::create();

		for ($i=0; $i < 40 ; $i++) { 
			Product::create([
				'name'  =>  $faker->catchPhrase, 
				'price' => $faker->numberBetween($min = 200, $max = 1800)
			]);
		} 
	}
}
