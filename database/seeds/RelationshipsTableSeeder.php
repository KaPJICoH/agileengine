<?php

use Illuminate\Database\Seeder;
use App\Product;
use App\Voucher;

class RelationshipsTableSeeder extends Seeder
{
    private $vouchers_id;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function __construct(){
        $this->faker = Faker\Factory::create();
        $this->vouchers_id = $this->getVouchersId();        
    }

    private function getVouchersId()
    {
        $vouchers = Voucher::all();
        $vouchers_id = [];
        foreach ($vouchers as $voucher) {               
            array_push($vouchers_id , $voucher['id']);
        }
        return $vouchers_id;
    }

    public function run()
    {
        $faker = Faker\Factory::create();
        $products = Product::all();     
        foreach ($products as $product) {
            $this->addVoucherToProduct($product['id']);
        }           
    }

    private function addVoucherToProduct($product_id)
    {
        $vouncherCount = $this->faker->numberBetween($min = 0, $max = 3);               
        for ($i=0; $i < $vouncherCount ; $i++) { 
            DB::table('voucher_to_product')->insert([
                'voucher_id' => $this->getRandomVoucherId(), 
                'product_id' => $product_id
            ]);
        }
    }

    private function getRandomVoucherId()
    {
        return $this->faker->randomElement($this->vouchers_id);
    }
}
