<?php

use Illuminate\Database\Seeder;
use App\Discount;

class DiscontTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('discount')->delete();

        $discounts = [10,15,20,25];

        foreach ($discounts as $discount) {
        	Discount::create([
        		'discount' => $discount
        	]);
        }
    }
}
