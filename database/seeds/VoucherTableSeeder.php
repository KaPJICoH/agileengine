<?php

use Illuminate\Database\Seeder;
use App\Voucher;
use App\Discount;
use Carbon\Carbon;

class VoucherTableSeeder extends Seeder
{
	private $faker;
	private $discounts_id;
	
	public function __construct(){
		$this->faker = Faker\Factory::create();
		$this->discounts_id = $this->getDiscountsId();          
	}

	private function getDiscountsId()
	{
		$discounts = Discount::all();
		$discounts_id = [];
		foreach ($discounts as $discount) {             
			array_push($discounts_id , $discount['id']);
		}
		return $discounts_id;
	}

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('voucher')->delete();
			
		for ($i=0; $i < 16 ; $i++) {
			$date_start = $this->faker->dateTimeBetween('-1 years' , 'now');
			$date_end =  $this->faker->dateTimeBetween($date_start , '+1 years' ); 
			Voucher::create([
				'date_start'    =>  $date_start, 
				'date_end'      =>  $date_end,
				'discount_id'       =>  $this->getRandomDiscountId()
			]);
		}
	}

	private function getRandomDiscountId()
	{
		return $this->faker->randomElement($this->discounts_id);
	}
}
