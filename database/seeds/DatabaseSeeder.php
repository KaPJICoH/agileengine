<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$this->call('DiscontTableSeeder');//todo 50 voucher

        $this->command->info('Discont was added to database!');

        $this->call('ProductTableSeeder');//todo 50 voucher

        $this->command->info('Product was added to database!');

        $this->call('VoucherTableSeeder');//todo 10 voucher

        $this->command->info('Voucher was added to database!');

        $this->call('RelationshipsTableSeeder');//todo relateanships

        $this->command->info('Voucher was added to Product!');  
    }
}
